var config = {
	development : {
		url : '',
        MONGODB_URI:'',
        Local_MONGODB_URI:'mongodb://localhost:27017/kiran',
		akDatabase : {
			host : 'localhost',
			user : 'ak',
			password : 'kiran',
			database : 'kiran',
			connectionLimit : 100, //important
			debug    :  false
		},
		//server details
		server : {
			host : '127.0.0.1',
			port : '8899'
		},
        secret:'myNameIsKiran'
	},
	qa : {
		//url to be used in link generation
		url : '',
        MONGODB_URI:'',
        Local_MONGODB_URI:'',
		akDatabase : {
			host : 'localhost',
			user : 'ak',
			password : 'kiran',
			database : 'kiran',
			connectionLimit : 100, //important
			debug    :  false
		},
		//server details
		server : {
			host : '127.0.0.1',
			port : '8899'
		},
        secret:'myNameIsKiran'
	},
	production : {
		url : '',
        MONGODB_URI:'',
        Local_MONGODB_URI:'mongodb://localhost:27017/kiran',
		akDatabase : {
			host : 'localhost',
			user : 'ak',
			password : 'kiran',
			database : 'kiran',
			connectionLimit : 100, //important
			debug    :  false
		},
		//server details
		server : {
			host : '127.0.0.1',
			port : '8899'
		},
        secret:'myNameIsKiran'
	}
};

module.exports = config;
